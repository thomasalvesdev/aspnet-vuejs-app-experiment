import Vue from 'vue';
import App from './components/App.vue';
import './style.scss';

const string = 'string';

const app = new Vue({
  el: '#app',
  data() {
    return {
      message: 'hello world!!!'
    };
  },
  render: h => h(App)
});
